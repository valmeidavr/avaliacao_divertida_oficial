
@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title','Alunos')

{{-- styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/vendors.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/ui/prism.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/extensions/toastr.css')}}">
@endsection

@section('content')

@if ($message = Session::get('success'))
<script>
    window.onload = function(e){
        Swal.fire('', '<?php echo $message ?>', 'success');
   };
   </script>
@endif
        <div class="content-header-left col-12 mb-2 mt-1">
          <div class="row breadcrumbs-top">
            <div class="col-12">
              <h5 class="content-header-title float-left pr-1 mb-0">Cadastro</h5>
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb p-0 mb-0">
                  <li class="breadcrumb-item"><a href="/"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active">Alunos
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>

<!-- Tabela  -->
<div class="row">
    <div class="col-12">
      <div class="card">
            <div class="card-header">

            </div>

        <div class="card-content">
          <!-- table head dark -->
          <div class="table-responsive">

            <table class="table mb-0">
              <thead class="thead-dark">
                <tr>
                  <th >Nome</th>
                  <th> E-mail </th>
                  <th width='150'>Ações</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($alunos as $aluno)
                    <tr>
                        <td><?php echo $aluno->name ?></td>
                        <td><?php echo $aluno->email ?></td>
                        <td>Editar | Apagar</td>
                    </tr>
                @endforeach

              </tbody>
            </table>


                    <div class="d-flex">
                        <div class="mx-auto">

                        </div>
                    </div>
          </div>
        </div>
       </div>
    </div>
  </div>
<!-- fim da tabela -->
@endsection

{{-- scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/extensions/toastr.min.js')}}"></script>
<script src="{{asset('vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('vendors/js/ui/prism.min.js')}}"></script>
@endsection
