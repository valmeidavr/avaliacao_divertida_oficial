<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaliacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacaos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('ano');

            $table->unsignedBigInteger('tipo_jogos_id');
            $table->foreign('tipo_jogos_id')->references('id')->on('tipo_jogos');

            $table->unsignedBigInteger('disciplinas_id');
            $table->foreign('disciplinas_id')->references('id')->on('disciplinas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacaos');
    }
}
