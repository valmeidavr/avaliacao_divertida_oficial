<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessorTurmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professor_turmas', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('professores_id');
            $table->foreign('professores_id')->references('id')->on('users');

            $table->unsignedBigInteger('turmas_id');
            $table->foreign('turmas_id')->references('id')->on('turmas');

            $table->unsignedBigInteger('disciplinas_id');
            $table->foreign('disciplinas_id')->references('id')->on('disciplinas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professor_turmas');
    }
}
