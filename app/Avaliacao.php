<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $fillable = [
        'nome',
        'ano',
        'tipo_jogos_id',
        'disciplinas_id'
    ];
}
