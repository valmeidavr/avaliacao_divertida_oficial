<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Exceptions\Handler;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalPage = 2;
        $usuarios = User::orderBy('name', 'asc')->paginate($totalPage);
        return view('pages.usuarios.index')->with('usuarios', $usuarios);
    }

    public function cadastrar() {
        $roles = Role::pluck('name','name')->all();
        return view('pages.usuarios.cadastrar',compact('roles'));
    }

    public function salvar(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $roles = Role::pluck('name','name')->all();
        return redirect()->route('usuarios.index')->with('success','Usuário cadastrado com successo.');
    }

    public function editar($id) {
        $usuario = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $usuario->roles->pluck('name','name')->all();
        return view('pages.usuarios.editar',compact('usuario','roles','userRole'));
    }

    public function atualizar(Request $request, $id) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        } else{
            $input = array_except($input, ['password']);
        }

        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('usuarios.index')->with('success','Usuário atualizado com successo.');

    }

    public function deletar($id) {
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            User::find($id)->delete();
            return redirect()->route('usuarios.index')->with('success','Usuário deletado com successo.');

    }

    public function logout() {
        \Auth::logout();
        return redirect()->route('login')->with('message', 'A sessão deste usuário expirou.');
    }

    public function pesquisar(Request $request) {
        $dataForm = $request->except('_token');
        $totalPage = 2;
        $name = $request->nome;
        $email = $request->email;

        if ((isset($name)) || (isset($email))) {
            $usuarios = User::where('name','like','%'.$name.'%')
              ->where('email','like','%'.$email.'%')
              ->orderBy('name','ASC')
              ->paginate($totalPage);
        } else {
            $usuarios = User::orderBy('name','ASC')->paginate($totalPage);
        }

        return view('pages.usuarios.index',compact('usuarios'))
        ->with('i', ($request->input('page', 1) - 1) * 5)
        ->with('dataForm',$dataForm);
    }
}
