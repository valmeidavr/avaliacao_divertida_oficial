<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class AlunosController extends Controller
{
    public function index () {
        $alunos = DB::select("SELECT * FROM users where status = 'aluno'");
        return view('pages.alunos.index', compact('alunos'));
    }
}
