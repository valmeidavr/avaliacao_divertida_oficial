<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessorTurma extends Model
{
    protected $fillable = [
        'professores_id',
        'turmas_id',
        'disciplinas_id'
    ];
}
