<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questao extends Model
{
    protected $fillable = [
        'pergunta',
        'resposta',
        'avaliacaos_id'
    ];
}
