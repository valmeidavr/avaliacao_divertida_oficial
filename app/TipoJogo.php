<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoJogo extends Model
{
    protected $fillable = [
        'nome'
    ];
}
